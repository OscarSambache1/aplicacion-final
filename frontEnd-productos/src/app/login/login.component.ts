import { Component } from "@angular/core";
import { AutenticacionService } from "../autenticacion/autenticacion.service";

@Component({
    selector:'login',
    templateUrl: './login.component.html',
    styleUrls:['./login.component.css']
})

export class LoginComponent{
    constructor(private readonly _autenticacionService: AutenticacionService){

    }
    Login(){

        this._autenticacionService.login('oscar@gmail.es', 'Oscar*29')
    }
    
}