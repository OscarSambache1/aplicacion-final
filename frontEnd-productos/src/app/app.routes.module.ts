import {Route, RouterModule} from '@angular/router'
import { NgModule } from '@angular/core';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { RutaNoEncontradaComponent } from './ruta-no-encontrada/ruta-no-encontrada';
import { GuardEstaLogueado } from './autenticacion/esta-logueado.guard';
import { AutenticacionService } from './autenticacion/autenticacion.service';

const rutas: Route[]=[
    {
        path:'inicio',
        component: InicioComponent,
        canActivate: [GuardEstaLogueado]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },

    {
        path:'usuario',
        loadChildren:'src/app/usuario/usuario.module#UsuarioModule'
      },
    {
        path:'**',
        component: RutaNoEncontradaComponent
    },
  
    

]

@NgModule({
    imports:[RouterModule.forRoot(rutas, {useHash:true})],
    exports:[RouterModule]
})

export class AppRoutingModule{
    
    
}

