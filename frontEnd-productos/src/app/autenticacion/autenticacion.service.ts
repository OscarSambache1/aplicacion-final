import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()

export class AutenticacionService{
    estaLogueado: boolean= false

    constructor(private readonly _httpClient: HttpClient){

    }
    probarServicio(){
        console.log('hola')
    }

    login(usuario: string, password:string){
        const credenciales= {
            usuario, 
            password
        }
        this._httpClient.post(environment.url+'/autenticacion/login', credenciales)
        .subscribe(usuarioLogueado=>{
            console.log('usuario', usuarioLogueado)
            this.estaLogueado=true
        },
        error=>{
            console.error('error', error)
            this.estaLogueado=false
        })
    }
}