import { NgModule } from "@angular/core";
import { AutenticacionService } from "./autenticacion.service";
import { GuardEstaLogueado } from "./esta-logueado.guard";
import {HttpClientModule} from "@angular/common/http"
@NgModule({
    declarations:[],
    providers:[AutenticacionService, GuardEstaLogueado],
    imports:[HttpClientModule],
    exports:[]
})

export class AutenticacionModule{
    
}