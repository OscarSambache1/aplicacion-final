import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, Router } from "@angular/router";
import { AutenticacionService } from "./autenticacion.service";

@Injectable()
export class GuardEstaLogueado implements CanActivate {

    constructor(private readonly _route:Router,
        private readonly _autenticacionService: AutenticacionService){
        
    }
    canActivate(parametroRuta : ActivatedRouteSnapshot){
        console.log('esta logueado',this._autenticacionService.estaLogueado)
        const estaLogueado=this._autenticacionService.estaLogueado
        if(estaLogueado){
            //this._route.navigate(['/inicio'])
            return true     
        }   
        else{
            alert('fallo login')
            this._route.navigate(['/login'])
            return false
        }

    }
}