import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from './autenticacion/autenticacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontEnd-productos';

  constructor(private readonly _AutemtnicacionService: AutenticacionService){

  }
  ngOnInit(){
    this._AutemtnicacionService.probarServicio()
  }
}
