import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class UsuarioService{
    arregloUsuarios:any
    constructor(private readonly _httpClient: HttpClient){

    }

    obtenerTodos():Observable<any>{
       return  this._httpClient.get(environment.url+'/usuario')
        
    }
}