import { NgModule } from '@angular/core';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioService } from './usuario.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule({
    imports: [
        UsuarioRoutingModule ,
        FormsModule,
        CommonModule
    ],
    declarations: [
        ListarUsuarioComponent,

    ],
    providers: [UsuarioService],
    exports:[]
})
export class UsuarioModule { }
