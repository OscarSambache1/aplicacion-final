import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "../usuario.service";
import {FormsModule, NgModel, NgForm} from '@angular/forms'



@Component({
    selector: 'listar-usuario',
    templateUrl: './listar-usuario.component.html',
    styleUrls: ['./listar-usuario.component.css']
})

export class ListarUsuarioComponent implements OnInit {
    public arregloUsuarios :any[]
    

    constructor(private readonly _usuarioService: UsuarioService){
        this.arregloUsuarios=[]
    }

    ngOnInit(){
       
    }

    listar(){
     this._usuarioService.obtenerTodos().subscribe(usuarios=>{
         this.arregloUsuarios=usuarios
         console.log('arreglo usuarios',this.arregloUsuarios)
     })
   
    }
}