import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity('_usuario')


export class UsuarioEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({name:'usuario_correo', type: 'varchar', unique:true})
    correo: string

    @Column({name:'usuario_password', type: 'varchar'})
    password: string

    @Column({name:'usuario_nombre', type: 'varchar'})
    nombre:string

}