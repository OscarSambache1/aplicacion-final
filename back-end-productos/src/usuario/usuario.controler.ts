import { Controller, Get, Param, Post, Body, Put, Delete, BadRequestException } from "@nestjs/common";
import { UsuarioService } from "./usuario.service";
import { generarUsuarioLogin } from "./funciones/generar-usuario-loguin";
import { crearUsuario } from "./funciones/crear-usuario";
import { validate } from "class-validator";
import { UsuarioEntity } from "./usuario.entity";

@Controller('usuario')

export class UsuarioController{
    constructor(private readonly _usuarioService: UsuarioService){

    }

    @Get()
    async obtenerTodos() {
        return await this._usuarioService.buscarTodos()
    }

    @Get(':id')
    obtenerUno(
        @Param('id') id
    ){
        return this._usuarioService.buscarPorId(id)
    }



    @Post()
    async crear(
        @Body('nombre') nombre,
        @Body('correo') correo ,
        @Body('password') password
        ){

        const usuarioACrearse = crearUsuario(correo, password, nombre)
        const arregloErrores = await validate(usuarioACrearse)
        const existenErrores = arregloErrores.length > 0
        console.log(arregloErrores)
        if(existenErrores){
            console.error('errores: creando al usuario',arregloErrores)
            throw new BadRequestException('Parametros incorrectos')
        }
        else{
            
                return  this._usuarioService.crear(usuarioACrearse)
            
        }

        
    }

    @Put(':id')
    editar(
        @Param() id: number,
        @Body() usuario
    ){
        return this._usuarioService.editar(id, usuario)
    }

    @Delete(':id')
    eliminar(
    @Param('id') id:number
    ){
        return this._usuarioService.eliminar(id)
    }
}