import { Module } from "@nestjs/common";
import { UsuarioController } from "./usuario.controler";
import { UsuarioService } from "./usuario.service";
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioEntity } from "./usuario.entity";
import { AutenticacionController } from "./autenticacion.controller";


@Module({
    controllers:[UsuarioController, AutenticacionController],
    imports:[TypeOrmModule.forFeature(
        [UsuarioEntity], 'conecion'
    )],
    providers:[UsuarioService]
})

export class UsuarioModule {

}