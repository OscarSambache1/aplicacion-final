import { UsuarioCrearDto } from "../dto/usuario-crear.dto";

export function crearUsuario(correo: string, password:string, nombre:string) :UsuarioCrearDto{
    const usuarioACrear = new UsuarioCrearDto()
    usuarioACrear.correo=correo,
    usuarioACrear.password=password
    usuarioACrear.nombre=nombre
    return usuarioACrear
}