import { Injectable } from "@nestjs/common";
import { InjectRepository} from "@nestjs/typeorm"
import { UsuarioEntity } from "./usuario.entity";
import { Repository, FindOneOptions } from "typeorm";

@Injectable()
export class UsuarioService{
    constructor(
        @InjectRepository(UsuarioEntity)
        private readonly _usuarioRepository: Repository<UsuarioEntity>
    ){

    }

    async buscarTodos (): Promise<UsuarioEntity[]>{
        console.log('tipo', typeof await this._usuarioRepository.find())
        return await this._usuarioRepository.find()
    }

    async buscarPorId (id: number){
        return await this._usuarioRepository.findOne(id)
    }

    async buscarPorEmail (correo: string): Promise<UsuarioEntity>{
        const opciones: FindOneOptions ={
            where:{
                correo,
                
            }
        }

        const usuarioEncontrado = this._usuarioRepository.findOne(undefined, opciones)
        return usuarioEncontrado
    }


    async crear (usuario: any){
        let serieCreada = await this._usuarioRepository.create(usuario)
        return await this._usuarioRepository.save(serieCreada)
    }

    async editar(id: number, usuario: any){
        await this._usuarioRepository.update(id, usuario)
        return this.buscarPorId(id)

    }

    async eliminar(id:number){
        let usuarioAEliminar = await this.buscarPorId(id)
        await this._usuarioRepository.remove(usuarioAEliminar)
        return usuarioAEliminar

    }
}