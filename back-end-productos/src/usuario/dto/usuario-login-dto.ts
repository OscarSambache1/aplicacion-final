
import {IsNotEmpty, IsEmail, Matches, IsString} from 'class-validator'
export class UsuarioLoginDto{
    @IsNotEmpty()
    @IsEmail()
    usuario: string

    @IsNotEmpty()
    @IsString()
    //@Matches(/^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#\$%\^&\*])(?=.{8,})/)    
    //@Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)
    @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/)
    password: string
}