import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from 'usuario/usuario-module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioEntity } from 'usuario/usuario.entity';

@Module({
  imports: [UsuarioModule,
  TypeOrmModule.forRoot({
    type: 'mysql',
      name: 'conecion',
      //host: '192.168.100.9',
      host: 'localhost',
      port: 32773,
      //username: 'admin',
      username: 'admin',
      password: '12345678',
      database: 'productos',
      entities: [UsuarioEntity],
      synchronize: true,

  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
